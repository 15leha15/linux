// SPDX-License-Identifier: GPL-2.0+
/*
 * Copyright (C) 2020 Linaro Ltd
 * Author: Sumit Semwal <sumit.semwal@linaro.org>
 */

#include <linux/delay.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_device.h>

#include <linux/gpio/consumer.h>
#include <linux/pinctrl/consumer.h>
#include <linux/regulator/consumer.h>

#include <drm/drm_device.h>
#include <drm/drm_mipi_dsi.h>
#include <drm/drm_modes.h>
#include <drm/drm_panel.h>
#include <drm/drm_print.h>

#include <video/mipi_display.h>

struct panel_cmd {
	size_t len;
	const char *data;
};

#define _INIT_CMD(...) { \
	.len = sizeof((char[]){__VA_ARGS__}), \
	.data = (char[]){__VA_ARGS__} }

static const char * const regulator_names[] = {
	"vddio",
	"vddpos",
	"vddneg",
};

static unsigned long const regulator_enable_loads[] = {
	62000,
	100000,
	100000
};

static unsigned long const regulator_disable_loads[] = {
	80,
	100,
	100
};

struct panel_desc {
	const struct drm_display_mode *display_mode;
	const char *panel_name;

	unsigned int width_mm;
	unsigned int height_mm;

	unsigned long mode_flags;
	enum mipi_dsi_pixel_format format;
	unsigned int lanes;

	const struct panel_cmd *on_cmds_1;
	const struct panel_cmd *on_cmds_2;

	const struct panel_cmd *off_cmds;
};

struct panel_info {
	struct drm_panel base;
	struct mipi_dsi_device *link;
	const struct panel_desc *desc;

	struct regulator_bulk_data supplies[ARRAY_SIZE(regulator_names)];

	struct gpio_desc *reset_gpio;

	struct pinctrl *pinctrl;
	struct pinctrl_state *active;
	struct pinctrl_state *suspend;


	bool prepared;
	bool enabled;
};

static inline struct panel_info *to_panel_info(struct drm_panel *panel)
{
	return container_of(panel, struct panel_info, base);
}

static int send_mipi_cmds(struct drm_panel *panel, const struct panel_cmd *cmds)
{
	struct panel_info *pinfo = to_panel_info(panel);
	unsigned int i = 0;
	int err;

	if (!cmds)
		return -EFAULT;

	for (i = 0; cmds[i].len != 0; i++) {
		const struct panel_cmd *cmd = &cmds[i];

		if (cmd->len == 2)
			err = mipi_dsi_dcs_write(pinfo->link,
						    cmd->data[1], NULL, 0);
		else
			err = mipi_dsi_dcs_write(pinfo->link,
						    cmd->data[1], cmd->data + 2,
						    cmd->len - 2);

		if (err < 0)
			return err;

	}

	return 0;
}

static int panel_set_pinctrl_state(struct panel_info *panel, bool enable)
{
	int rc = 0;
	struct pinctrl_state *state;

	if (enable)
		state = panel->active;
	else
		state = panel->suspend;

	rc = pinctrl_select_state(panel->pinctrl, state);
	if (rc)
		pr_err("[%s] failed to set pin state, rc=%d\n", panel->desc->panel_name,
			rc);
	return rc;
}

static int novatek_panel_disable(struct drm_panel *panel)
{
	struct panel_info *pinfo = to_panel_info(panel);

	pinfo->enabled = false;

	return 0;
}

static int novatek_panel_power_off(struct drm_panel *panel)
{
	struct panel_info *pinfo = to_panel_info(panel);
	int i, ret = 0;

	gpiod_set_value(pinfo->reset_gpio, 0);

	ret = panel_set_pinctrl_state(pinfo, false);
	if (ret) {
		pr_err("[%s] failed to set pinctrl, rc=%d\n", pinfo->desc->panel_name, ret);
		return ret;
	}

	for (i = 0; i < ARRAY_SIZE(pinfo->supplies); i++) {
		ret = regulator_set_load(pinfo->supplies[i].consumer,
				regulator_disable_loads[i]);
		if (ret) {
			DRM_DEV_ERROR(panel->dev,
				"regulator_set_load failed %d\n", ret);
			return ret;
		}
	}

	ret = regulator_bulk_disable(ARRAY_SIZE(pinfo->supplies), pinfo->supplies);
	if (ret) {
		DRM_DEV_ERROR(panel->dev,
			"regulator_bulk_disable failed %d\n", ret);
	}
	return ret;
}

static int novatek_panel_unprepare(struct drm_panel *panel)
{
	struct panel_info *pinfo = to_panel_info(panel);
	int ret;

	if (!pinfo->prepared)
		return 0;

	/* send off cmds */
	ret = send_mipi_cmds(panel, pinfo->desc->off_cmds);

	if (ret < 0) {
		DRM_DEV_ERROR(panel->dev,
				"failed to send DCS off cmds: %d\n", ret);
	}

	ret = mipi_dsi_dcs_set_display_off(pinfo->link);
	if (ret < 0) {
		DRM_DEV_ERROR(panel->dev,
			"set_display_off cmd failed ret = %d\n",
			ret);
	}

	/* 120ms delay required here as per DCS spec */
	msleep(120);

	ret = mipi_dsi_dcs_enter_sleep_mode(pinfo->link);
	if (ret < 0) {
		DRM_DEV_ERROR(panel->dev,
			"enter_sleep cmd failed ret = %d\n", ret);
	}
	/* 0x3C = 60ms delay */
	msleep(60);

	ret = novatek_panel_power_off(panel);
	if (ret < 0)
		DRM_DEV_ERROR(panel->dev, "power_off failed ret = %d\n", ret);

	pinfo->prepared = false;

	return ret;

}

static int novatek_panel_power_on(struct panel_info *pinfo)
{
	int ret, i;

	for (i = 0; i < ARRAY_SIZE(pinfo->supplies); i++) {
		ret = regulator_set_load(pinfo->supplies[i].consumer,
					regulator_enable_loads[i]);
		if (ret)
			return ret;
	}

	ret = regulator_bulk_enable(ARRAY_SIZE(pinfo->supplies), pinfo->supplies);
	if (ret < 0)
		return ret;

	ret = panel_set_pinctrl_state(pinfo, true);
	if (ret) {
		pr_err("[%s] failed to set pinctrl, rc=%d\n", pinfo->desc->panel_name, ret);
		return ret;
	}

	/*
	 * As per downstream kernel, Reset sequence of Tianma nt36672a panel requires the panel to
	 * be out of reset for 10ms, followed by being held in reset for 10ms. But for Android
	 * AOSP, we needed to bump it upto 200ms otherwise we get white screen sometimes.
	 * FIXME: Try to reduce this 200ms to a lesser value.
	 */
	gpiod_set_value(pinfo->reset_gpio, 0);
	msleep(200);
	gpiod_set_value(pinfo->reset_gpio, 1);
	msleep(200);

	return 0;
}

static int novatek_panel_prepare(struct drm_panel *panel)
{
	struct panel_info *pinfo = to_panel_info(panel);
	int err;

	if (pinfo->prepared)
		return 0;

	err = novatek_panel_power_on(pinfo);
	if (err < 0)
		goto poweroff;

	/* send first part of init cmds */
	err = send_mipi_cmds(panel, pinfo->desc->on_cmds_1);

	if (err < 0) {
		DRM_DEV_ERROR(panel->dev,
				"failed to send DCS Init 1st Code: %d\n", err);
		goto poweroff;
	}

	err = mipi_dsi_dcs_set_display_on(pinfo->link);
	if (err < 0) {
		DRM_DEV_ERROR(panel->dev,
				"failed to Set Display ON: %d\n", err);
		goto poweroff;
	}

	err = mipi_dsi_dcs_exit_sleep_mode(pinfo->link);
	if (err < 0) {
		DRM_DEV_ERROR(panel->dev, "failed to exit sleep mode: %d\n",
			      err);
		goto poweroff;
	}
	/* 0x46 = 70 ms delay */
	msleep(70);

	/* Send rest of the init cmds */
	err = send_mipi_cmds(panel, pinfo->desc->on_cmds_2);

	if (err < 0) {
		DRM_DEV_ERROR(panel->dev,
				"failed to send DCS Init 2nd Code: %d\n", err);
		goto poweroff;
	}

	msleep(120);

	pinfo->prepared = true;

	return 0;

poweroff:
	gpiod_set_value(pinfo->reset_gpio, 1);
	return err;
}


static int novatek_panel_enable(struct drm_panel *panel)
{
	struct panel_info *pinfo = to_panel_info(panel);

	if (pinfo->enabled)
		return 0;

	pinfo->enabled = true;

	return 0;
}

static int novatek_panel_get_modes(struct drm_panel *panel,
				struct drm_connector *connector)
{
	struct panel_info *pinfo = to_panel_info(panel);
	const struct drm_display_mode *m = pinfo->desc->display_mode;
	struct drm_display_mode *mode;

	mode = drm_mode_duplicate(connector->dev, m);
	if (!mode) {
		DRM_DEV_ERROR(panel->dev, "failed to add mode %ux%u@%u\n",
				m->hdisplay, m->vdisplay, drm_mode_vrefresh(m));
		return -ENOMEM;
	}

	connector->display_info.width_mm = pinfo->desc->width_mm;
	connector->display_info.height_mm = pinfo->desc->height_mm;

	drm_mode_set_name(mode);
	drm_mode_probed_add(connector, mode);

	return 1;
}

static const struct drm_panel_funcs panel_funcs = {
	.disable = novatek_panel_disable,
	.unprepare = novatek_panel_unprepare,
	.prepare = novatek_panel_prepare,
	.enable = novatek_panel_enable,
	.get_modes = novatek_panel_get_modes,
};

static const struct panel_cmd novatek_nt35596s_on_cmds_1[] = {
	_INIT_CMD(0x00, 0xff, 0x24),
	_INIT_CMD(0x00, 0x9d, 0x34),
	_INIT_CMD(0x00, 0xfb, 0x01),
	_INIT_CMD(0x00, 0xc4, 0x25),
	_INIT_CMD(0x00, 0xd1, 0x08),
	_INIT_CMD(0x00, 0xd2, 0x84),
	_INIT_CMD(0x00, 0xff, 0x26),
	_INIT_CMD(0x00, 0xfb, 0x01),
	_INIT_CMD(0x00, 0x03, 0x1c),
	_INIT_CMD(0x00, 0x3b, 0x08),
	_INIT_CMD(0x00, 0x6b, 0x08),
	_INIT_CMD(0x00, 0x97, 0x08),
	_INIT_CMD(0x00, 0xc5, 0x08),
	_INIT_CMD(0x00, 0xfb, 0x01),
	_INIT_CMD(0x00, 0xff, 0x23),
	_INIT_CMD(0x00, 0xfb, 0x01),
	_INIT_CMD(0x00, 0x01, 0x84),
	_INIT_CMD(0x00, 0x05, 0x2d),
	_INIT_CMD(0x00, 0x06, 0x00),
	_INIT_CMD(0x00, 0x32, 0x00),
	_INIT_CMD(0x00, 0x13, 0xff),
	_INIT_CMD(0x00, 0x14, 0xf8),
	_INIT_CMD(0x00, 0x15, 0xed),
	_INIT_CMD(0x00, 0x16, 0xe5),
	_INIT_CMD(0x00, 0x09, 0x01),
	_INIT_CMD(0x00, 0x0a, 0x01),
	_INIT_CMD(0x00, 0x0b, 0x01),
	_INIT_CMD(0x00, 0x0c, 0x01),
	_INIT_CMD(0x00, 0x0d, 0x01),
	_INIT_CMD(0x00, 0x0e, 0x01),
	_INIT_CMD(0x00, 0x0f, 0x01),
	_INIT_CMD(0x00, 0x10, 0x01),
	_INIT_CMD(0x00, 0x11, 0x01),
	_INIT_CMD(0x00, 0x12, 0x01),
	_INIT_CMD(0x00, 0x17, 0xff),
	_INIT_CMD(0x00, 0x18, 0xee),
	_INIT_CMD(0x00, 0x19, 0xdd),
	_INIT_CMD(0x00, 0x1a, 0xc7),
	_INIT_CMD(0x00, 0x1b, 0xaf),
	_INIT_CMD(0x00, 0x1c, 0x99),
	_INIT_CMD(0x00, 0x1d, 0x99),
	_INIT_CMD(0x00, 0x1e, 0x88),
	_INIT_CMD(0x00, 0x1f, 0x77),
	_INIT_CMD(0x00, 0x20, 0x66),
	_INIT_CMD(0x00, 0x33, 0x00),
	_INIT_CMD(0x00, 0x21, 0xff),
	_INIT_CMD(0x00, 0x22, 0xf8),
	_INIT_CMD(0x00, 0x23, 0xef),
	_INIT_CMD(0x00, 0x24, 0xe7),
	_INIT_CMD(0x00, 0x25, 0xde),
	_INIT_CMD(0x00, 0x26, 0xd7),
	_INIT_CMD(0x00, 0x27, 0xcd),
	_INIT_CMD(0x00, 0x28, 0xc4),
	_INIT_CMD(0x00, 0x29, 0xbc),
	_INIT_CMD(0x00, 0x2a, 0xb3),
	_INIT_CMD(0x00, 0xff, 0x22),
	_INIT_CMD(0x00, 0x00, 0x0a),
	_INIT_CMD(0x00, 0x01, 0x43),
	_INIT_CMD(0x00, 0x02, 0x5b),
	_INIT_CMD(0x00, 0x03, 0x6a),
	_INIT_CMD(0x00, 0x04, 0x7a),
	_INIT_CMD(0x00, 0x05, 0x82),
	_INIT_CMD(0x00, 0x06, 0x85),
	_INIT_CMD(0x00, 0x07, 0x80),
	_INIT_CMD(0x00, 0x08, 0x7c),
	_INIT_CMD(0x00, 0x09, 0x7c),
	_INIT_CMD(0x00, 0x0a, 0x74),
	_INIT_CMD(0x00, 0x0b, 0x71),
	_INIT_CMD(0x00, 0x0c, 0x6e),
	_INIT_CMD(0x00, 0x0d, 0x68),
	_INIT_CMD(0x00, 0x0e, 0x65),
	_INIT_CMD(0x00, 0x0f, 0x5c),
	_INIT_CMD(0x00, 0x10, 0x32),
	_INIT_CMD(0x00, 0x11, 0x18),
	_INIT_CMD(0x00, 0x12, 0x00),
	_INIT_CMD(0x00, 0x13, 0x00),
	_INIT_CMD(0x00, 0x1a, 0x00),
	_INIT_CMD(0x00, 0x1b, 0x00),
	_INIT_CMD(0x00, 0x1c, 0x00),
	_INIT_CMD(0x00, 0x1d, 0x00),
	_INIT_CMD(0x00, 0x1e, 0x00),
	_INIT_CMD(0x00, 0x1f, 0x00),
	_INIT_CMD(0x00, 0x20, 0x00),
	_INIT_CMD(0x00, 0x21, 0x00),
	_INIT_CMD(0x00, 0x22, 0x00),
	_INIT_CMD(0x00, 0x23, 0x00),
	_INIT_CMD(0x00, 0x24, 0x00),
	_INIT_CMD(0x00, 0x25, 0x00),
	_INIT_CMD(0x00, 0x26, 0x00),
	_INIT_CMD(0x00, 0x27, 0x00),
	_INIT_CMD(0x00, 0x28, 0x00),
	_INIT_CMD(0x00, 0x29, 0x00),
	_INIT_CMD(0x00, 0x2a, 0x00),
	_INIT_CMD(0x00, 0x2b, 0x00),
	_INIT_CMD(0x00, 0x2f, 0x00),
	_INIT_CMD(0x00, 0x30, 0x00),
	_INIT_CMD(0x00, 0x31, 0x00),
	_INIT_CMD(0x00, 0x32, 0x0c),
	_INIT_CMD(0x00, 0x33, 0x0c),
	_INIT_CMD(0x00, 0x34, 0x0c),
	_INIT_CMD(0x00, 0x35, 0x0b),
	_INIT_CMD(0x00, 0x36, 0x09),
	_INIT_CMD(0x00, 0x37, 0x09),
	_INIT_CMD(0x00, 0x38, 0x08),
	_INIT_CMD(0x00, 0x39, 0x05),
	_INIT_CMD(0x00, 0x3a, 0x03),
	_INIT_CMD(0x00, 0x3b, 0x00),
	_INIT_CMD(0x00, 0x3f, 0x00),
	_INIT_CMD(0x00, 0x40, 0x00),
	_INIT_CMD(0x00, 0x41, 0x00),
	_INIT_CMD(0x00, 0x42, 0x00),
	_INIT_CMD(0x00, 0x43, 0x00),
	_INIT_CMD(0x00, 0x44, 0x00),
	_INIT_CMD(0x00, 0x45, 0x00),
	_INIT_CMD(0x00, 0x46, 0x00),
	_INIT_CMD(0x00, 0x47, 0x00),
	_INIT_CMD(0x00, 0x48, 0x00),
	_INIT_CMD(0x00, 0x49, 0x03),
	_INIT_CMD(0x00, 0x4a, 0x06),
	_INIT_CMD(0x00, 0x4b, 0x07),
	_INIT_CMD(0x00, 0x4c, 0x07),
	_INIT_CMD(0x00, 0x4d, 0x00),
	_INIT_CMD(0x00, 0x4e, 0x00),
	_INIT_CMD(0x00, 0x4f, 0x00),
	_INIT_CMD(0x00, 0x50, 0x00),
	_INIT_CMD(0x00, 0x51, 0x00),
	_INIT_CMD(0x00, 0x52, 0x00),
	_INIT_CMD(0x00, 0x53, 0x01),
	_INIT_CMD(0x00, 0x54, 0x01),
	_INIT_CMD(0x00, 0x55, 0x89),
	_INIT_CMD(0x00, 0x56, 0x00),
	_INIT_CMD(0x00, 0x58, 0x00),
	_INIT_CMD(0x00, 0x68, 0x00),
	_INIT_CMD(0x00, 0x84, 0xff),
	_INIT_CMD(0x00, 0x85, 0xff),
	_INIT_CMD(0x00, 0x86, 0x03),
	_INIT_CMD(0x00, 0x87, 0x00),
	_INIT_CMD(0x00, 0x88, 0x00),
	_INIT_CMD(0x00, 0xa2, 0x20),
	_INIT_CMD(0x00, 0xa9, 0x01),
	_INIT_CMD(0x00, 0xaa, 0x12),
	_INIT_CMD(0x00, 0xab, 0x13),
	_INIT_CMD(0x00, 0xac, 0x0a),
	_INIT_CMD(0x00, 0xad, 0x74),
	_INIT_CMD(0x00, 0xaf, 0x33),
	_INIT_CMD(0x00, 0xb0, 0x03),
	_INIT_CMD(0x00, 0xb1, 0x14),
	_INIT_CMD(0x00, 0xb2, 0x42),
	_INIT_CMD(0x00, 0xb3, 0x40),
	_INIT_CMD(0x00, 0xb4, 0xa5),
	_INIT_CMD(0x00, 0xb6, 0x44),
	_INIT_CMD(0x00, 0xb7, 0x04),
	_INIT_CMD(0x00, 0xb8, 0x14),
	_INIT_CMD(0x00, 0xb9, 0x42),
	_INIT_CMD(0x00, 0xba, 0x40),
	_INIT_CMD(0x00, 0xbb, 0xa5),
	_INIT_CMD(0x00, 0xbd, 0x44),
	_INIT_CMD(0x00, 0xbe, 0x04),
	_INIT_CMD(0x00, 0xbf, 0x00),
	_INIT_CMD(0x00, 0xc0, 0x75),
	_INIT_CMD(0x00, 0xc1, 0x6a),
	_INIT_CMD(0x00, 0xc2, 0xa5),
	_INIT_CMD(0x00, 0xc4, 0x22),
	_INIT_CMD(0x00, 0xc5, 0x02),
	_INIT_CMD(0x00, 0xc6, 0x00),
	_INIT_CMD(0x00, 0xc7, 0x95),
	_INIT_CMD(0x00, 0xc8, 0x8a),
	_INIT_CMD(0x00, 0xc9, 0xa5),
	_INIT_CMD(0x00, 0xcb, 0x22),
	_INIT_CMD(0x00, 0xcc, 0x02),
	_INIT_CMD(0x00, 0xcd, 0x00),
	_INIT_CMD(0x00, 0xce, 0xb5),
	_INIT_CMD(0x00, 0xcf, 0xaa),
	_INIT_CMD(0x00, 0xd0, 0xa5),
	_INIT_CMD(0x00, 0xd2, 0x22),
	_INIT_CMD(0x00, 0xd3, 0x02),
	_INIT_CMD(0x00, 0xfb, 0x01),
	_INIT_CMD(0x00, 0xff, 0x10),
	_INIT_CMD(0x00, 0x26, 0x02),
	_INIT_CMD(0x00, 0x35, 0x00),
	_INIT_CMD(0x00, 0x51, 0xff),
	_INIT_CMD(0x00, 0x53, 0x24),
	_INIT_CMD(0x00, 0x55, 0x00),
	_INIT_CMD(0x00, 0xb0, 0x00),

	{},
};

static const struct panel_cmd novatek_nt35596s_on_cmds_2[] = {

};

static const struct panel_cmd novatek_nt35596s_off_cmds[] = {

};

static const struct drm_display_mode novatek_panel_default_mode = {
	.clock		= 153298,

	.hdisplay	= 1080,
	.hsync_start	= 1080 + 16,
	.hsync_end	= 1080 + 16 + 28,
	.htotal		= 1080 + 16 + 28 + 40,

	.vdisplay	= 2160,
	.vsync_start	= 2160 + 7,
	.vsync_end	= 2160 + 7 + 4,
	.vtotal		= 2160 + 7 + 4 + 24,

	.type = DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED,
};

static const struct panel_desc novatek_panel_desc = {
	.display_mode = &novatek_panel_default_mode,

	.width_mm = 68,
	.height_mm = 136,

	.mode_flags = MIPI_DSI_MODE_LPM | MIPI_DSI_MODE_VIDEO
			| MIPI_DSI_MODE_VIDEO_HSE
			| MIPI_DSI_CLOCK_NON_CONTINUOUS
			| MIPI_DSI_MODE_VIDEO_BURST,
	.format = MIPI_DSI_FMT_RGB888,
	.lanes = 4,
	.on_cmds_1 = novatek_nt35596s_on_cmds_1,
	.on_cmds_2 = novatek_nt35596s_on_cmds_2,
	.off_cmds = novatek_nt35596s_off_cmds
};


static const struct of_device_id panel_of_match[] = {
	{ .compatible = "novatek,nt35596s",
	  .data = &novatek_panel_desc
	},
	{
		/* sentinel */
	}
};
MODULE_DEVICE_TABLE(of, panel_of_match);


static int panel_pinctrl_init(struct panel_info *panel)
{
	struct device *dev = &panel->link->dev;
	int rc = 0;

	panel->pinctrl = devm_pinctrl_get(dev);
	if (IS_ERR_OR_NULL(panel->pinctrl)) {
		rc = PTR_ERR(panel->pinctrl);
		pr_err("failed to get pinctrl, rc=%d\n", rc);
		goto error;
	}

	panel->active = pinctrl_lookup_state(panel->pinctrl,
							"panel_active");
	if (IS_ERR_OR_NULL(panel->active)) {
		rc = PTR_ERR(panel->active);
		pr_err("failed to get pinctrl active state, rc=%d\n", rc);
		goto error;
	}

	panel->suspend =
		pinctrl_lookup_state(panel->pinctrl, "panel_suspend");

	if (IS_ERR_OR_NULL(panel->suspend)) {
		rc = PTR_ERR(panel->suspend);
		pr_err("failed to get pinctrl suspend state, rc=%d\n", rc);
		goto error;
	}

error:
	return rc;
}

static int panel_add(struct panel_info *pinfo)
{
	struct device *dev = &pinfo->link->dev;
	int i, ret;

	for (i = 0; i < ARRAY_SIZE(pinfo->supplies); i++)
		pinfo->supplies[i].supply = regulator_names[i];

	ret = devm_regulator_bulk_get(dev, ARRAY_SIZE(pinfo->supplies),
				      pinfo->supplies);
	if (ret < 0)
		return ret;

	pinfo->reset_gpio = devm_gpiod_get(dev, "reset", GPIOD_OUT_HIGH);
	if (IS_ERR(pinfo->reset_gpio)) {
		DRM_DEV_ERROR(dev, "cannot get reset gpio %ld\n",
			PTR_ERR(pinfo->reset_gpio));
		return PTR_ERR(pinfo->reset_gpio);
	}

	ret = panel_pinctrl_init(pinfo);
	if (ret < 0)
		return ret;

	drm_panel_init(&pinfo->base, dev, &panel_funcs,
		       DRM_MODE_CONNECTOR_DSI);

	drm_panel_add(&pinfo->base);
	return 0;
}

static void panel_del(struct panel_info *pinfo)
{
	if (pinfo->base.dev)
		drm_panel_remove(&pinfo->base);
}

static int panel_probe(struct mipi_dsi_device *dsi)
{
	struct panel_info *pinfo;
	const struct panel_desc *desc;
	int err;

	pinfo = devm_kzalloc(&dsi->dev, sizeof(*pinfo), GFP_KERNEL);
	if (!pinfo)
		return -ENOMEM;

	desc = of_device_get_match_data(&dsi->dev);
	dsi->mode_flags = desc->mode_flags;
	dsi->format = desc->format;
	dsi->lanes = desc->lanes;
	pinfo->desc = desc;
	pinfo->link = dsi;

	mipi_dsi_set_drvdata(dsi, pinfo);

	err = panel_add(pinfo);
	if (err < 0)
		return err;

	err = mipi_dsi_attach(dsi);
	return err;
}

static int panel_remove(struct mipi_dsi_device *dsi)
{
	struct panel_info *pinfo = mipi_dsi_get_drvdata(dsi);
	int err;

	err = novatek_panel_unprepare(&pinfo->base);
	if (err < 0)
		DRM_DEV_ERROR(&dsi->dev, "failed to unprepare panel: %d\n",
				err);

	err = novatek_panel_disable(&pinfo->base);
	if (err < 0)
		DRM_DEV_ERROR(&dsi->dev, "failed to disable panel: %d\n", err);

	err = mipi_dsi_detach(dsi);
	if (err < 0)
		DRM_DEV_ERROR(&dsi->dev, "failed to detach from DSI host: %d\n",
				err);

	panel_del(pinfo);

	return 0;
}

static void panel_shutdown(struct mipi_dsi_device *dsi)
{
	struct panel_info *pinfo = mipi_dsi_get_drvdata(dsi);

	novatek_panel_disable(&pinfo->base);
	novatek_panel_unprepare(&pinfo->base);
}

static struct mipi_dsi_driver panel_driver = {
	.driver = {
		.name = "panel-novatek-nt35596s",
		.of_match_table = panel_of_match,
	},
	.probe = panel_probe,
	.remove = panel_remove,
	.shutdown = panel_shutdown,
};
module_mipi_dsi_driver(panel_driver);

MODULE_AUTHOR("Sumit Semwal <sumit.semwal@linaro.org>");
MODULE_DESCRIPTION("NOVATEK NT35596s MIPI-DSI LCD panel");
MODULE_LICENSE("GPL");
